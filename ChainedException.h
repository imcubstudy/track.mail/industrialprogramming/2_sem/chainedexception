#ifndef TRACK_CPP_2_SEM_CHAINEDEXCEPTION_H
#define TRACK_CPP_2_SEM_CHAINEDEXCEPTION_H

#include <exception>
#include <string>
#include <limits.h>

class ChainedException: public std::exception
{
    friend struct ChainedExceptionHelper;
    
private:
    ChainedException*   _prev   = nullptr;
    
protected:
    static constexpr const char*  _typename   = "ChainedException";
    
public:
    enum excpt_descriptor_t
    {
        UNKNOWN         = INT_MAX - 0,
        FUNC_FAILURE    = INT_MAX - 1,
        STD_EXCPT       = INT_MAX - 2,
        BAD_ARG         = INT_MAX - 3,
        CONSTRUCTION    = INT_MAX - 4,
        INVAVID_OBJ     = INT_MAX - 5,
        MEM_ERR         = INT_MAX - 6
    };
    
protected:
    int                 _descriptor = UNKNOWN;
    const char*         _what       = nullptr;
    
    struct assisted_t
    {
        bool        flag    = false;
        void*       helper  = nullptr;
    } _assisted;
    
public:
    virtual const char* what() const noexcept override;
    
    ChainedException() noexcept = default;
    ChainedException( const ChainedException& that ) noexcept;
    ChainedException( const char* what, int descriptor = UNKNOWN,
                     ChainedException* prev = nullptr,
                     assisted_t assisted = { false, nullptr } ) noexcept;
    ~ChainedException() noexcept;
    
    size_t  dump( FILE* out_file ) const;
    
    bool assisted() const noexcept
    { return _assisted.flag; }
    
    int descriptor() const noexcept
    { return _descriptor; }
};

#ifndef _$CHAINED_EXCEPTION_MESSAGE_BUFFER_SIZE
    #define _$CHAINED_EXCEPTION_MESSAGE_BUFFER_SIZE (16 * 1024) // 16 kilobytes by default
#endif

#ifndef _$CHAINED_EXCEPTION_OBJECT_BUFFER_SIZE
    #define _$CHAINED_EXCEPTION_OBJECT_BUFFER_SIZE 1024 // 1024 exception objects by default
#endif

struct ChainedExceptionHelper
{
private:
    char*       _obj_buffer[_$CHAINED_EXCEPTION_OBJECT_BUFFER_SIZE]     = {};
    char        _msg_buffer[_$CHAINED_EXCEPTION_MESSAGE_BUFFER_SIZE]    = {};
    
    size_t      _obj_free       = 0;
    size_t      _msg_free       = 0;
    
public:
    enum    prev_expt_type_t
    {
        STD     = 0,
        CHAINED = 1
    };
    
    bool                release_resources( ChainedException* e ) noexcept;
    
    ChainedException*   get_excpt( const char* what,
                                  int descriptor = ChainedException::UNKNOWN,
                                  ChainedException* prev = nullptr ) noexcept;
    
    const char*         get_msg( const char* func, const char* reason,
                                prev_expt_type_t prev_type ) noexcept;
};

extern ChainedExceptionHelper __chained_excpt_h;

#define chainable( code )                                                       \
{                                                                               \
    try                                                                         \
    {                                                                           \
        code                                                                    \
    }                                                                           \
    catch( ChainedException *e )                                                \
    {                                                                           \
        throw __chained_excpt_h.get_excpt(                                      \
              __chained_excpt_h.get_msg( __PRETTY_FUNCTION__, e->what(),        \
                                         ChainedExceptionHelper::CHAINED ),     \
                                         ChainedException::FUNC_FAILURE, e );   \
    }                                                                           \
    catch( const std::exception& e )                                            \
    {                                                                           \
        throw __chained_excpt_h.get_excpt(                                      \
              __chained_excpt_h.get_msg( __PRETTY_FUNCTION__, e.what(),         \
                                            ChainedExceptionHelper::STD ),      \
                                            ChainedException::STD_EXCPT );      \
    }                                                                           \
}

#define chained_throw( reason, descriptor )                                     \
    throw   __chained_excpt_h.get_excpt( reason,  descriptor  )                 \

#define drop_chain( e )                                                         \
do                                                                              \
{                                                                               \
    if( e->assisted() )                                                         \
    {                                                                           \
        __chained_excpt_h.release_resources( e );                               \
    }                                                                           \
    else                                                                        \
    {                                                                           \
        delete e;                                                               \
    }                                                                           \
} while( false )                                                                \

#endif //TRACK_CPP_2_SEM_CHAINEDEXCEPTION_H
