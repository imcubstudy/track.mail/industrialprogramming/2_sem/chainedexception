#include "ChainedException.h"
#include <iostream>
#include <fstream>

void thrower( u_int64_t recurrent_factor = 0 ) chainable
({
    if( recurrent_factor <= 1 )
        throw __chained_excpt_h.get_excpt( "Hello" );
    
    else thrower( recurrent_factor - 1 );
})

void catcher( u_int64_t recurrent_factor = 0 ) chainable
({
    thrower( recurrent_factor );
})

int main()
{
    try
    {
        catcher( 3 );
    }
    catch( ChainedException* e )
    {
        FILE* out = fopen("Result.txt", "w");
        e->dump( out );
        std::cout << e->what() << "\n";
        fclose(out);
        
        drop_chain(e);
    }
    
    return 0;
}
