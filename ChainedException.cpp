#include "ChainedException.h"
#include <fstream>

ChainedExceptionHelper __chained_excpt_h = {};

const char* ChainedException::what() const noexcept
{
    return _what ? _what : _typename;
}

ChainedException::ChainedException( const ChainedException &that ) noexcept
{
    _descriptor     = that._descriptor;
    
    if( that._what != nullptr )
    {
        size_t      len     = strlen( that._what );
        
        char*       dummy   = new (std::nothrow) char[len]{};
        
        if( dummy != nullptr )
        {
            std::strcpy( dummy, that._what );
            _what = dummy;
        }
        else _what = "ChainedException copy";
            }
    
    if( that._prev != nullptr )
    {
        _prev = new (std::nothrow) ChainedException( *(that._prev) );
    }
}

ChainedException::ChainedException( const char *what,
                                   int descriptor,
                                   ChainedException *prev,
                                   assisted_t assisted ) noexcept:
_what( what ),
_prev( prev ),
_assisted( assisted ),
_descriptor( descriptor )
{}

ChainedException::~ChainedException() noexcept
{
    if( _assisted.flag )
    {
        ((ChainedExceptionHelper*)_assisted.helper)->release_resources(this);
    }
    else
    {
        delete[]    _what;
    }
    
    if( _prev != nullptr )
    {
        if( !(_prev->assisted()) )
            delete _prev;
    }
}


size_t  ChainedException::dump( FILE* out_file ) const
{
    fprintf( out_file, "%s\n", what() );
    size_t  iter    = 1;
    
    ChainedException*   runner = _prev;
    while( runner != nullptr )
    {
        for( size_t i = 0; i < iter; ++i )
            fprintf( out_file, "\t" );
        
        if( runner->_prev == nullptr )
            fprintf( out_file, "%s\n", runner->what() );
        else fprintf( out_file, "%s because\n", runner->what() );
        
        iter++;
        runner  = runner->_prev;
    }
    return iter;
}

const char* ChainedExceptionHelper::get_msg( const char *func, const char *reason,
                                            ChainedExceptionHelper::prev_expt_type_t prev_type ) noexcept
{
    char    dummy[100]  = {};
    
    switch( prev_type )
    {
        case CHAINED:
        {
            sprintf( dummy, " failed" );
            
            size_t  total_len = strlen( func ) + strlen( dummy );
            
            if( _msg_free + total_len  + 1 < _$CHAINED_EXCEPTION_MESSAGE_BUFFER_SIZE )
            {
                char    *result = _msg_buffer + _msg_free;
                
                sprintf( result, "%s%s", func, dummy );
                _msg_free += total_len + 1;
                
                
                return result;
            }
            
            break;
        }
        case STD:
        {
            sprintf( dummy, " failed because std::exception.what() = " );
            
            size_t  total_len = strlen( func ) + strlen( dummy )
            + strlen( reason );
            
            if( _msg_free + total_len + 1 < _$CHAINED_EXCEPTION_MESSAGE_BUFFER_SIZE )
            {
                char    *result = _msg_buffer + _msg_free;
                
                sprintf( result, "%s%s%s", func, dummy, reason );
                _msg_free += total_len + 1;
                
                return result;
            }
            
            break;
        }
    }
    
    return "ChainedExceptionHelper buffer overflow";
}

ChainedException* ChainedExceptionHelper::get_excpt( const char* what,
                                                    int descriptor,
                                                    ChainedException* prev ) noexcept
{
    if( _obj_free + 1 < _$CHAINED_EXCEPTION_OBJECT_BUFFER_SIZE )
    {
        _obj_free++;
        
        return new( _obj_buffer + (_obj_free - 1) * sizeof(ChainedException) )
        ChainedException( what, descriptor, prev, { true, this } );
    }
    
    return new (std::nothrow) ChainedException( what, descriptor, prev );
}

bool ChainedExceptionHelper::release_resources( ChainedException *e ) noexcept
{
    if( e->_assisted.helper == this )
    {
        _obj_free--;
        _msg_free -= strlen( e->what() ) + 1;
        
        return true;
    }
    return false;
}

